#!/bin/bash
if [[ ! -z "$GNU_MIRROR" ]]; then
  GSL_DOWNLOAD_URL="$GNU_MIRROR/gsl/gsl-2.6.tar.gz"
else
  GSL_DOWNLOAD_URL="https://ftpmirror.gnu.org/gsl/gsl-2.6.tar.gz"
fi

if [[ ! -z "$GNOME_MIRROR" ]]; then
  GLIB_DOWNLOAD_URL="$GNOME_MIRROR/sources/glib/2.62/glib-2.62.0.tar.xz"
else
  GLIB_DOWNLOAD_URL="https://download.gnome.org/sources/glib/2.62/glib-2.62.0.tar.xz"
fi

# download, build and install gsl
curl -L $GSL_DOWNLOAD_URL -o gsl-2.6.tar.gz
tar -xzf gsl-2.6.tar.gz
rm gsl-2.6.tar.gz
cd gsl-2.6
./configure && make && make install
cd ..
rm -r gsl-2.6
PATH=/opt/python/cp37-cp37m/bin:$PATH
# install xz
if [[ ! -z "$PIP_MIRROR" ]]; then
  curl -L https://mirrors.tuna.tsinghua.edu.cn/centos/6.10/os/x86_64/Packages/xz-4.999.9-0.5.beta.20091007git.el6.x86_64.rpm -o xz.rpm
  rpm -i xz.rpm
  rm xz.rpm
else
  yum install -y xz
fi
# install meson and ninja
if [[ -z "$PIP_MIRROR" ]]; then
  PIP_MIRROR="https://pypi.python.org/simple"
fi
  pip install -i $PIP_MIRROR ninja meson

# download, build and install static glib
curl -L $GLIB_DOWNLOAD_URL -o glib-2.62.0.tar.xz
tar -xJf glib-2.62.0.tar.xz
rm glib-2.62.0.tar.xz
cd glib-2.62.0
meson setup --buildtype release --default-library static -D libmount=false _build
ninja -C _build install
cd ..
# install cython
pip install -i $PIP_MIRROR cython
/opt/python/cp36-cp36m/bin/pip install -i $PIP_MIRROR cython
