FROM quay.io/pypa/manylinux2010_x86_64:latest
COPY build.sh /build_scripts/
ARG GNU_MIRROR
ENV GNU_MIRROR=$GNU_MIRROR
ARG PIP_MIRROR
ENV PIP_MIRROR=$PIP_MIRROR
ARG GNOME_MIRROR
ENV GNOME_MIRROR=$GNOME_MIRROR
RUN cd /build_scripts && bash build.sh && rm build.sh
CMD ["/bin/bash"]

